import {
  moduleName
} from './const.js';
import {
  registerSettings
} from './configuration/settings.js';
import {
  playSfxVfx
} from './lib/playSfxVfx.js';
import {
  createTab
} from './lib/ItemCustomTab.js';

Hooks.once('init', async function() {
  //register settings
  registerSettings();
});

//Hook on item sheet render to put our tab into it
Hooks.on(`renderItemSheet`, (app, html, item) => {
  let acceptedTypes = ['weapon', 'gear', 'skill', 'power', 'ability', 'shield'];
  if (acceptedTypes.includes(item.data.type)) {
    createTab(app, html, item);
  }
});

// Base System
Hooks.on('swadeAction', (swadeActor, swadeItem, actionType, roll, id) => {
  if (actionType === "formula") {
    playSfxVfx(swadeItem);
  }
});

//betterroll case with hooks
Hooks.on('BRSW-RollItem', (message, html) => {
  // get actor (Code from betterrolls-swade2)
  let actor = null;
  if (canvas) {
    if (message.getFlag('betterrolls-swade2', 'token')) {
      let token = canvas.tokens.get(message.getFlag('betterrolls-swade2', 'token'));
      if (token) actor = token.actor
    } else {
      // If we couldn't get the token, maybe because it was not defined actor.
      if (message.getFlag('betterrolls-swade2', 'actor')) {
        actor = game.actors.get(message.getFlag('betterrolls-swade2', 'actor'));
      }
    }
  }
  // get item id
  let item_id = message.getFlag('betterrolls-swade2', 'item_id');
  // get item
  let swadeItem = actor.items.find((item) => item.id === item_id);

  playSfxVfx(swadeItem);
});

// Swade tools case
Hooks.on('renderChatMessage', (message, html, data) => {
  // if is a roll of a skill
  if (message.isRoll && message.getFlag('swade-tools', 'rolltype') === 'skill') {
    // get the item id
    let itemId = message.getFlag('swade-tools', 'itemroll');
    // get the token id
    let tokenId = message.getFlag('swade-tools', 'usetoken');
    // get the actor id
    let actorId = message.getFlag('swade-tools', 'useactor');
    // get the item
    let swadeItem;
    try {
      if (tokenId) {
        swadeItem = canvas.tokens.get(tokenId).actor.items.find((item) => item.id === itemId);
      } else {
        if (actorId) {
          swadeItem = game.actors.get(actorId).items.find((item) => item.id === itemId);
        } else {
          // how can i get the item???
          console.log("what??!?!?");
        }
      }

      playSfxVfx(swadeItem);
    } catch (e) {
      console.debug("Controlled Error in swade-sfx-and-vfx-enchantments and SWADE tools");
    }
  }
});
