import {
  moduleName
} from '../const.js';
import {
  genTable
} from '../utils/pagination.js';

export class animationTemplatesConfiguration extends FormApplication {

  // get the default options
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      id: 'animationTemplate-settings-form',
      title: game.i18n.localize('SWADESFXVFX.animationTemplateTitle'),
      template: './modules/' + moduleName + '/templates/animationTemplate.html',
      closeOnSubmit: true
    });
  }

  // get the data for the page
  getData() {
    return game.settings.get(moduleName, 'animationTemplateList');
  }

  //create the listeners
  activateListeners(html) {
    //call the super listeners
    super.activateListeners(html);

    // generate the pagination
    if (html.get()[0].ownerDocument.getElementById("animationTemplateList") != null) {
      genTable(html.get()[0].ownerDocument.getElementById("animationTemplateList"), "4");
    }

    var animationTemplateList = game.settings.get(moduleName, 'animationTemplateList');
    let ventana = this;

    //capture the change on the scale X select
    $("#animationTemplateXScale").change(function() {
      if (document.getElementById("animationTemplateXScale").value === 'manual') {
        document.getElementById("animationTemplateXScaleManual").disabled = false;
      } else {
        document.getElementById("animationTemplateXScaleManual").disabled = true;
        document.getElementById("animationTemplateXScaleManual").value = '';
      }
    });

    //capture the change in the scale Y select
    $("#animationTemplateYScale").change(function() {
      if (document.getElementById("animationTemplateYScale").value === 'manual') {
        document.getElementById("animationTemplateYScaleManual").disabled = false;
      } else {
        document.getElementById("animationTemplateYScaleManual").disabled = true;
        document.getElementById("animationTemplateYScaleManual").value = '';
      }
    });

    //Capture the export the settings event
    html.find('.swadesfxvfxDownload').click(evt => {
      if (animationTemplateList) {
        saveDataToFile(JSON.stringify(animationTemplateList), "text/json", "animation_templateList_settings.json");
      } else {
        ui.notifications.info(game.i18n.localize("SWADESFXVFX.NoData"));
      }
    });

    //capture the drag and drop event for the import settings
    const dragDrop = new DragDrop({
      dropSelector: ".animationListImport",
      callbacks: {
        drop: async function(event) {
          var reader = new FileReader();
          reader.onloadend = async function() {
            var data = JSON.parse(this.result);
            //save data
            await game.settings.set(moduleName, 'animationTemplateList', data);
            ventana.render(true);
            genTable(html.get()[0].ownerDocument.getElementById("animationTemplateList"), "4");
          };
          // read the file
          reader.readAsText(event.dataTransfer.files[0]);
          ui.notifications.info(game.i18n.localize("SWADESFXVFX.importingAnimationTemplate"));
          //event.preventDefault();
        }
      }
    });
    dragDrop.bind(html[0]);

    //capture the delete template event
    html.find('.swadesfxvfsDelete').click(async evt => {
      animationTemplateList.animationTemplateList.splice(parseInt(evt.currentTarget.id.replace("SWADESFXVFX-delete-animation-template-", ""), 10), 1);
      await game.settings.set(moduleName, 'animationTemplateList', animationTemplateList);
      ventana.render(true);
      genTable(html.get()[0].ownerDocument.getElementById("animationTemplateList"), "4");
    });

  }

  // update the object (create a new template)
  async _updateObject(event, formData) {
    //get the aved config data
    var animationTemplateList = game.settings.get(moduleName, 'animationTemplateList');
    if (formData.animationTemplateName.trim() !== "" && formData.animationTemplateFile.trim() !== "") {

      let found = false;
      // find if already exist to update
      animationTemplateList.animationTemplateList.forEach(element => {
        if (formData.animationTemplateName === element.animationTemplateName && element.animationTemplateName != ' ') {
          //Update
          element.animationTemplateFile = formData.animationTemplateFile;
          element.animationTemplateXScale = formData.animationTemplateXScale;
          element.animationTemplateXScaleManual = formData.animationTemplateXScaleManual;
          element.animationTemplateYScale = formData.animationTemplateYScale;
          element.animationTemplateYScaleManual = formData.animationTemplateYScaleManual;
          element.animationTemplateTarget = formData.animationTemplateTarget;
          element.animationTemplateSpeed = formData.animationTemplateSpeed;
          element.animationTemplateAnchorX = formData.animationTemplateAnchorX;
          element.animationTemplateAnchorY = formData.animationTemplateAnchorY;
          element.animationTemplateAngle = formData.animationTemplateAngle;
          found = true;
        }
      });
      if (!found) {
        //creeate new
        animationTemplateList.animationTemplateList.push(formData);
      }
      await game.settings.set(moduleName, 'animationTemplateList', animationTemplateList);
    } else {
      console.log("swade-sfx-vfx can not create empty template");
    }
  }
}
