import {
  moduleName
} from '../const.js';
import {
  genTable
} from '../utils/pagination.js';

export class audioTemplatesConfiguration extends FormApplication {

  // get the default options
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      id: 'audioTemplate-settings-form',
      title: game.i18n.localize('SWADESFXVFX.audioTemplateTitle'),
      template: './modules/' + moduleName + '/templates/audioTemplate.html',
      closeOnSubmit: true
    });
  }

  // get the data for the page
  getData() {
    return game.settings.get(moduleName, 'audioTemplateList');
  }

  //create the listeners
  activateListeners(html) {
    //call the super listeners
    super.activateListeners(html);

    // generate the pagination
    if (html.get()[0].ownerDocument.getElementById("audioTemplateList") != null) {
      genTable(html.get()[0].ownerDocument.getElementById("audioTemplateList"), "5");
    }

    var audioTemplateList = game.settings.get(moduleName, 'audioTemplateList');
    let ventana = this;

    //Download the settings event
    html.find('.swadesfxvfxDownload').click(evt => {
      if (audioTemplateList) {
        saveDataToFile(JSON.stringify(audioTemplateList), "text/json", "audio_templateList_settings.json");
      } else {
        ui.notifications.info(game.i18n.localize("SWADESFXVFX.NoData"));
      }
    });

    //capture the drag and drop event for the import settings
    const dragDrop = new DragDrop({
      dropSelector: ".audioListImport",
      callbacks: {
        drop: async function(event) {
          var reader = new FileReader();
          reader.onloadend = async function() {
            var data = JSON.parse(this.result);
            //save data
            await game.settings.set(moduleName, 'audioTemplateList', data);
            ventana.render(true);
            genTable(html.get()[0].ownerDocument.getElementById("audioTemplateList"), "5");
          };
          // read the file
          reader.readAsText(event.dataTransfer.files[0]);
          ui.notifications.info(game.i18n.localize("SWADESFXVFX.importingAudioTemplate"));
        }
      }
    });
    dragDrop.bind(html[0]);

    //capture the delete template event
    html.find('.swadesfxvfsDelete').click(async evt => {
      console.log("aqui estoy");
      audioTemplateList.audioTemplateList.splice(parseInt(evt.currentTarget.id.replace("SWADESFXVFX-delete-audio-template-", ""), 10), 1);
      await game.settings.set(moduleName, 'audioTemplateList', audioTemplateList);
      ventana.render(true);
      genTable(html.get()[0].ownerDocument.getElementById("audioTemplateList"), "5");
    });
  }

  // update the object (create a new template)
  async _updateObject(event, formData) {

    var audioTemplateList = game.settings.get(moduleName, 'audioTemplateList');

    if (formData.audioTemplateName.trim() !== "" && formData.audioTemplateFile.trim() !== "") {
      let found = false;
      // find if already exist to update
      audioTemplateList.audioTemplateList.forEach(element => {
        if (formData.audioTemplateName === element.audioTemplateName && element.audioTemplateName != ' ') {
          //Update
          element.audioTemplateVolume = formData.audioTemplateVolume;
          element.audioTemplateFile = formData.audioTemplateFile;
          found = true;
        }
      });
      if (!found) {
        //create new
        audioTemplateList.audioTemplateList.push(formData);
      }
      game.settings.set(moduleName, 'audioTemplateList', audioTemplateList);
    } else {
      console.log("swade-sfx-vfx can not create empty template");
    }
  }
}
