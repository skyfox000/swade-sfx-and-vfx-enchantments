import {
    moduleName
} from '../const.js';
import {
    audioTemplatesConfiguration
} from './audioTemplatesConfiguration.js';
import {
    animationTemplatesConfiguration
} from './animationTemplatesConfiguration.js';
import {
    playAnimation
} from '../lib/playSfxVfx.js'

export function registerSettings() {

    game.swadesfxafx = {};
    game.swadesfxafx.playAnimation = playAnimation;

    game.settings.register(moduleName, 'audioTemplateList', {
        name: "Audio template list",
        default: {
            audioTemplateList: []
        },
        type: Object,
        scope: "world",
        config: false,
        restricted: true
    });

    game.settings.register(moduleName, 'animationTemplateList', {
        name: "Animation template list",
        default: {
            animationTemplateList: []
        },
        type: Object,
        scope: "world",
        config: false,
        restricted: true
    });

    game.settings.registerMenu(moduleName, 'sfxVfxAudioTemplatesMenu', {
        name: "SWADESFXVFX.audiosTemplateName",
        label: "SWADESFXVFX.audiosTemplateLabel",
        hint: "SWADESFXVFX.audiosTemplateHint",
        icon: "fas fa-file-audio", // A Font Awesome icon used in the submenu button
        type: audioTemplatesConfiguration,
        restricted: true
    });

    game.settings.registerMenu(moduleName, 'sfxVfxAnimationsTemplatesMenu', {
        name: "SWADESFXVFX.animationsTemplateName",
        label: "SWADESFXVFX.animationsTemplateLabel",
        hint: "SWADESFXVFX.animationsTemplateHint",
        icon: "fas fa-file-video", // A Font Awesome icon used in the submenu button
        type: animationTemplatesConfiguration,
        restricted: true
    });

    //var audioTemplateList = game.settings.get(moduleName, 'audioTemplateList');

    /*var noneTemplate = [{
        audioTemplateName: game.i18n.localize('SWADESFXVFX.none'),
        audioTemplateFile: "",
        audioTemplateVolume: ""
    }]*/

    /*const hit_defaults = noneTemplate.concat(audioTemplateList);
    const raise_defaults = noneTemplate.concat(audioTemplateList);
    const miss_defaults = noneTemplate.concat(audioTemplateList);
    const fumble_defaults = noneTemplate.concat(audioTemplateList);*/

    /*game.settings.register(moduleName, 'sfxVfxDefaultHitSound', {
        name: game.i18n.localize('SWADESFXVFX.defaultHitSoundName'),
        hint: game.i18n.localize('SWADESFXVFX.defaultHitSoundHint'),
        default: game.i18n.localize('SWADESFXVFX.none'),
        scope: "world",
        type: String,
        choices: hit_defaults,
        config: true
    });

    game.settings.register(moduleName, 'sfxVfxDefaultRaiseSound', {
        name: game.i18n.localize('SWADESFXVFX.defaultRaiseSoundName'),
        hint: game.i18n.localize('SWADESFXVFX.defaultRaiseSoundHint'),
        default: game.i18n.localize('SWADESFXVFX.none'),
        scope: "world",
        type: String,
        choices: raise_defaults,
        config: true
    });

    game.settings.register(moduleName, 'sfxVfxDefaultMissSound', {
        name: game.i18n.localize('SWADESFXVFX.defaultMissSoundName'),
        hint: game.i18n.localize('SWADESFXVFX.defaultSMissSoundHint'),
        default: game.i18n.localize('SWADESFXVFX.none'),
        scope: "world",
        type: String,
        choices: miss_defaults,
        config: true
    });

    game.settings.register(moduleName, 'sfxVfxDefaultFumbleSound', {
        name: game.i18n.localize('SWADESFXVFX.defaultFumbleSoundName'),
        hint: game.i18n.localize('SWADESFXVFX.defaultFumbleSoundHint'),
        default: game.i18n.localize('SWADESFXVFX.none'),
        scope: "world",
        type: String,
        choices: fumble_defaults,
        config: true
    });*/

    //
    //

}

/*function testSound() {

}*/
