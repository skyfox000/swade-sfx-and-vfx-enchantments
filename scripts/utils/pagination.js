var perPage = 5;

//Function that generate pagination for table with colspan for the pages number in the last TD of the table
export function genTable(table, colspan) {
    perPage = parseInt(table.dataset.pagecount);
    createFooters(table, colspan);
    table.dataset.currentpage = "0";
    loadTable(table);
}

// based on current page, only show the elements in that range
export function loadTable(table) {
    var startIndex = 0;

    if (table.querySelector('th'))
        startIndex = 1;

    var start = (parseInt(table.dataset.currentpage) * table.dataset.pagecount) + startIndex;
    var end = start + parseInt(table.dataset.pagecount);
    var rows = table.rows;

    for (var x = startIndex; x < rows.length - 1; x++) {
        if (x < start || x >= end)
            rows[x].style.display = "none"
        else
            rows[x].style.display = "table-row"
    }
}

// Create the pagination footer
export function createFooters(table, colspan) {

    var hasHeader = false;
    if (table.querySelector('th'))
        hasHeader = true;

    var rows = table.rows.length;

    if (hasHeader)
        rows = rows - 1;

    var numPages = rows / perPage;
    var pagerTr = document.createElement("tr");
    var pagerTd = document.createElement("td");
    pagerTd.style.cssText = "text-align: right";
    pagerTd.colSpan = colspan;
    pagerTd.innerHTML = game.i18n.localize('SWADESFXVFX.pages');
    pagerTr.appendChild(pagerTd);

    if (numPages % 1 > 0)
        numPages = Math.floor(numPages) + 1;

    if (numPages > 1) {
        for (var i = 0; i < numPages; i++) {

            var page = document.createElement("div");
            page.innerHTML = i + 1;
            page.className = "pager-item";
            page.dataset.index = i;

            if (i == 0) {
                page.style.cssText = "background-color: #555555; border: none; color: white;  padding: 5px 5px; text-align: center; text-decoration: none; display: inline-block; font-size: 15px;";
            } else {
                page.style.cssText = "background-color: #e7e7e7; border: none; color: black;  padding: 5px 5px; text-align: center; text-decoration: none; display: inline-block; font-size: 15px;";
            }

            page.addEventListener('click', function() {
                var parent = this.parentNode;
                var items = parent.querySelectorAll(".pager-item");
                for (var x = 0; x < items.length; x++) {
                    items[x].style.cssText = "background-color: #e7e7e7; border: none; color: black;  padding: 5px 5px; text-align: center; text-decoration: none; display: inline-block; font-size: 15px;";
                }
                this.style.cssText = "background-color: #555555; border: none; color: white;  padding: 5px 5px; text-align: center; text-decoration: none; display: inline-block; font-size: 15px;";
                table.dataset.currentpage = this.dataset.index;
                loadTable(table);
            });
            pagerTd.appendChild(page);
        }
        // insert page at the bottom of the table in an extra TD
        table.getElementsByTagName('tbody')[0].appendChild(pagerTr);
    }


}
