import {
  moduleName
} from '../const.js';

// add token magic macros to the selection list
async function addMacrosList(macros, macrosToken, macrosTarget, item) {

  macros.forEach(element => {
    //remove the macros that dont do ny effect
    if (!element.name.includes("00") &&
      //!element.name.includes("A - ") &&
      //!element.name.includes("B - ") &&
      //!element.name.includes("C - ") &&
      //!element.name.includes("D - ") &&
      !element.name.includes("E - ") &&
      !element.name.includes("F - ")) {
      let macroToken = {
        name: element.name
      };
      let macroTarget = {
        name: element.name
      };
      if (item.data.flags.swadesfxvfx.tokenMagicData &&
        item.data.flags.swadesfxvfx.tokenMagicData.macrosToken === element.name) {
        macroToken.selected = true;
      }
      if (item.data.flags.swadesfxvfx.tokenMagicData &&
        item.data.flags.swadesfxvfx.tokenMagicData.macrosTarget === element.name) {
        macroTarget.selected = true;
      }
      macrosToken.push(macroToken);
      macrosTarget.push(macroTarget);
    }
  });
}

//get data
async function getData(item) {
  // get animation data
  let animationData = game.settings.get(moduleName, 'animationTemplateList');
  // if no animation data
  if (!animationData) {
    animationData = {
      animationTemplateList: []
    };
  }
  //create the default none value
  let noneAnimation = {
    animationTemplateName: " "
  };
  // set the animation data for the selection in the item sheet
  animationData.animationTemplateList.unshift(noneAnimation);
  // get audio data
  let audioData = game.settings.get(moduleName, 'audioTemplateList');
  // if no audio data
  if (!audioData) {
    audioData = {
      audioTemplateList: []
    };
  }
  //create the default none value
  let noneAudio = {
    audioTemplateName: " "
  };
  // set the audio data for the selection in the item sheet
  audioData.audioTemplateList.unshift(noneAudio);
  // if there are flags of previous selections
  if (item.data.flags.swadesfxvfx) {
    // put the selected to true for animation
    animationData.animationTemplateList.forEach(element => {
      // set the selected item in the combo box
      if (item.data.flags.swadesfxvfx.animationTemplate === element.animationTemplateName) {
        element.selected = true;
      }
    });
    // put the selected to true for audio
    audioData.audioTemplateList.forEach(element => {
      // set the selected item in the combo box
      if (item.data.flags.swadesfxvfx.audioTemplate === element.audioTemplateName) {
        element.selected = true;
      }
    });
  } else {
    //put the none as selected
    animationData.animationTemplateList[0].selected = true;
    audioData.audioTemplateList[0].selected = true;
  }

  // tokenMagic data is exist
  let tokenMagicData;
  if (game.packs.get("tokenmagic.tmMacros")) {
    //get the TokenMagic macros
    let macros_base = await game.packs.get("tokenmagic.tmMacros").getIndex();
    let macros_fire = await game.packs.get("tokenmagic.tmSampleCompendium").getIndex();

    //initialize the macros list
    let macrosToken = [{
      name: " "
    }];
    let macrosTarget = [{
      name: " "
    }];

    // if there are flags of previous selections
    if (item.data.flags.swadesfxvfx) {
      // base token magic macro list
      addMacrosList(macros_base, macrosToken, macrosTarget, item);
      // fire token magic macro list
      addMacrosList(macros_fire, macrosToken, macrosTarget, item);

      tokenMagicData = {
        macrosToken: macrosToken,
        macrosTarget: macrosTarget,
        macroTokenDelay: item.data.flags.swadesfxvfx.tokenMagicData ? item.data.flags.swadesfxvfx.tokenMagicData.macroTokenDelay : "",
        macroTargetDelay: item.data.flags.swadesfxvfx.tokenMagicData ? item.data.flags.swadesfxvfx.tokenMagicData.macroTargetDelay : "",
        macroTokenDuration: item.data.flags.swadesfxvfx.tokenMagicData ? item.data.flags.swadesfxvfx.tokenMagicData.macroTokenDuration : "",
        macroTargetDuration: item.data.flags.swadesfxvfx.tokenMagicData ? item.data.flags.swadesfxvfx.tokenMagicData.macroTargetDuration : "",
        macroTargetAnimationDuration: item.data.flags.swadesfxvfx.tokenMagicData ? item.data.flags.swadesfxvfx.tokenMagicData.macroTargetAnimationDuration : false,
        tokenMagic: true
      };
    } else {
      macrosToken[0].selected = true;
      macrosTarget[0].selected = true;
      tokenMagicData = {
        macrosToken: macrosToken,
        macrosTarget: macrosTarget,
        macroTokenDelay: "",
        macroTargetDelay: "",
        macroTokenDuration: "",
        macroTargetDuration: "",
        macroTargetAnimationDuration: false,
        tokenMagic: true
      };
    }

  } else {
    tokenMagicData = {
      tokenMagic: false
    };
  }

  // create the object with all the data
  let allData = {
    animationTemplates: animationData,
    audioTemplates: audioData,
    animationDelay: item.data.flags.swadesfxvfx ? item.data.flags.swadesfxvfx.animationDelay : "",
    audioDelay: item.data.flags.swadesfxvfx ? item.data.flags.swadesfxvfx.audioDelay : "",
    tokenMagicData: tokenMagicData
  }
  return allData;
}

// create the animation and sound tab in the item
export async function createTab(app, html, item) {
  //find the tabs div
  let tabs = html.find('.tabs');
  // attach new tab
  tabs.append($(
    '<a class="item" data-tab="' + moduleName + '">' + game.i18n.localize('SWADESFXVFX.animationAndSound') + '</a>'
  ));
  // find body and attach animation and sound div
  $(html.find('.sheet-body')).append($(
    '<div class="tab animation-and-sound-items" data-group="primary" data-tab="' + moduleName + '"></div>'
  ))

  //get the data to load in the tab
  let allData = await getData(item);
  //render the tab
  await renderTab(html, allData);

  //Download the settings event
  html.find('.swadesfxvfxDownload').click(evt => {
    if (item.data.flags.swadesfxvfx) {
      saveDataToFile(JSON.stringify(item.data.flags.swadesfxvfx), "text/json", "item_settings.json");
    } else {
      ui.notifications.info(game.i18n.localize("SWADESFXVFX.NoData"));
    }
  });

  //Upload the settings via drag and drop
  const dragDrop = new DragDrop({
    dropSelector: ".animation-and-sound-tab-contents",
    callbacks: {
      drop: async function(event) {
        var reader = new FileReader();
        reader.onloadend = async function() {
          var data = JSON.parse(this.result);
          //save data
          item.data.flags.swadesfxvfx = data;
          //get data
          let allData = await getData(item);
          // render the tab
          await renderTab(html, allData);
        };
        // read the file
        reader.readAsText(event.dataTransfer.files[0]);
        ui.notifications.info(game.i18n.localize("SWADESFXVFX.importing"));
        //event.preventDefault();
      }
    }
  });
  dragDrop.bind(html[0]);
}

// render the animation and sound tab
async function renderTab(html, allData) {
  //get template
  let temp = await renderTemplate('modules/' + moduleName + '/templates/itemSFX_AFX.html', allData);
  // find the tab content div
  let el = html.find('.animation-and-sound-tab-contents');
  // find the tab
  if (el.length) {
    //replace it
    el.replaceWith(temp);
  } else {
    //or attach it
    html.find('.tab.animation-and-sound-items').append(temp);
  }
  // when one of the select change
  html.find('.animation-and-sound-tab-contents').change(evt => {
    // check if this is necesary to not submit on change??
    return false;
  });
}
