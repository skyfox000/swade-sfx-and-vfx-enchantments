import {
  moduleName
} from '../const.js';
import {
  drawSpecialToward
} from './FXMasterCustom.js';

//Play the effects, animation and sounds
export async function playSfxVfx(SwadeItem) {
  //Get item sfxVfx config
  const sfxVfx = SwadeItem.data.flags.swadesfxvfx;
  // get the token
  const tokens = canvas.tokens.controlled;
  if (tokens.length == 0 || !sfxVfx) {
    // no token selected so exit or no config in item... so bye.
    return;
  }
  //get the tokenMagicData for the token and play it
  let macros;
  if (game.packs.get("tokenmagic.tmMacros") && sfxVfx.tokenMagicData) {
    macros = await game.packs.get("tokenmagic.tmMacros").getIndex();
    playTokenMagic(SwadeItem, macros, tokens);
  }
  //play audio
  playSound(sfxVfx);
  //get animatin delay
  let animationDelay = sfxVfx.animationDelay ? sfxVfx.animationDelay : 0;
  //play animation in token
  let durationAnimation = await playTokenTargetAnimation(sfxVfx, tokens, animationDelay);
  //get the tokenMagicData for the target(s) and play it
  if (game.packs.get("tokenmagic.tmMacros") && sfxVfx.tokenMagicData) {
    playTargetMagic(SwadeItem, macros, durationAnimation, animationDelay);
  }
}

// Play the tokenmagic effect in token if there is any
async function playTokenMagic(SwadeItem, macros, tokens) {

  const tokenMagicData = SwadeItem.data.flags.swadesfxvfx.tokenMagicData;
  //Find the macro data
  const found = macros.find(element => element.name === tokenMagicData.macrosToken);
  if (found) {
    //get and create the macro
    let macroDocument = await game.packs.get("tokenmagic.tmMacros").getDocument(found._id);
    let macro = new Macro(macroDocument.data);

    //find the delay and duration for the macro
    let delay = tokenMagicData.macroTokenDelay ? tokenMagicData.macroTokenDelay : 0;
    let duration = tokenMagicData.macroTokenDuration ? tokenMagicData.macroTokenDuration : 0;
    //set the delay for the tokenMagic animation execution
    const playMacroToken = function() {
      let params;
      //weird thing to use the params from the macro but not the execution of the animation
      eval(macro.data.command.replace('await TokenMagic.addUpdateFiltersOnSelected(params);', '')
        .replace('TokenMagic.addUpdateFiltersOnSelected(params);', '')
        .replace('await TokenMagic.addFiltersOnSelected(params);', '')
        .replace('TokenMagic.addFiltersOnSelected(params);', '')
        .replace('let params', 'params'));
      TokenMagic.addUpdateFilters(tokens[0], params);
    }
    //set the delays if any
    if (delay != 0) {
      setTimeout(playMacroToken, delay * 1000.0);
    } else {
      playMacroToken();
    }
    //set the duration of the tokenMagic animation
    const stopPlayMacroToken = function() {
      TokenMagic.deleteFilters(tokens[0]);
    }
    if (duration != 0) {
      setTimeout(stopPlayMacroToken, (parseFloat(delay) + parseFloat(duration)) * 1000.0);
    } else {
      // what to do if no duration is defined?
    }
  }
}

// Play the tokenmagic effect in target if there is any
async function playTargetMagic(SwadeItem, macros, durationAnimation, animationDelay) {

  const tokenMagicData = SwadeItem.data.flags.swadesfxvfx.tokenMagicData;
  //Find the macro data
  const found = macros.find(element => element.name === tokenMagicData.macrosTarget);

  if (found) {
    //get and create the macro
    let macroDocument = await game.packs.get("tokenmagic.tmMacros").getDocument(found._id);
    let macro = new Macro(macroDocument.data);

    //find the delay and duration for the macro
    let delay = tokenMagicData.macroTargetDelay ? tokenMagicData.macroTargetDelay : 0;
    let duration = tokenMagicData.macroTargetDuration ? tokenMagicData.macroTargetDuration : 0;

    //set the delay for the execution
    const playMacroTarget = function() {
      let params;
      //weird thing to use the params from the macro but not the execution of the animation
      eval(macro.data.command.replace('await TokenMagic.addUpdateFiltersOnSelected(params);', '')
        .replace('TokenMagic.addUpdateFiltersOnSelected(params);', '')
        .replace('await TokenMagic.addFiltersOnSelected(params);', '')
        .replace('TokenMagic.addFiltersOnSelected(params);', '')
        .replace('let params', 'params'));
      game.user.targets.forEach((i, t) => {
        TokenMagic.addUpdateFilters(t, params);
      });
    }
    let extraDelay = 0;
    if (tokenMagicData.macroTargetAnimationDuration) {
      extraDelay = parseFloat(durationAnimation) + parseFloat(animationDelay);
    }
    //set the delays if any
    if (delay != 0) {
      setTimeout(playMacroTarget, (parseFloat(delay) + extraDelay) * 1000.0);
    } else {
      if (extraDelay != 0) {
        setTimeout(playMacroTarget, extraDelay * 1000.0);
      } else {
        playMacroTarget();
      }
    }
    //set the duration of the animation
    const stopPlayMacroTarget = function() {
      game.user.targets.forEach((i, t) => {
        TokenMagic.deleteFilters(t);
      });
    }
    if (duration != 0) {
      setTimeout(stopPlayMacroTarget, (parseFloat(delay) + parseFloat(duration) + extraDelay) * 1000.0);
    } else {
      // what to do if no duration is defined?
    }
  }
}

//play the audio
export async function playSound(sfxVfx) {
  let audioSettings = game.settings.get(moduleName, 'audioTemplateList');
  // get the audio template to play
  let audioDelay = sfxVfx.audioDelay ? sfxVfx.audioDelay : 0;
  audioSettings.audioTemplateList.forEach(element => {
    if (sfxVfx.audioTemplate === element.audioTemplateName && element.audioTemplateName != ' ') {
      const playAudio = function() {
        var sonido = AudioHelper.play({
          src: element.audioTemplateFile,
          volume: element.audioTemplateVolume,
          autoplay: true,
          loop: false
        }, true);
      }
      if (audioDelay != 0) {
        setTimeout(playAudio, parseFloat(audioDelay) * 1000.0);
      } else {
        playAudio();
      }
    }
  });
}

// play animation in token and/or target
export async function playTokenTargetAnimation(sfxVfx, tokens, animationDelay) {
  // get the flags for the animation and audio templates
  let animationSettings = game.settings.get(moduleName, 'animationTemplateList');

  // get the animation template to play
  let durationAnimation = 0;
  let animationTemplateTarget;
  for (let i = 0; i < animationSettings.animationTemplateList.length; i++) {
    let element = animationSettings.animationTemplateList[i];
    if (sfxVfx.animationTemplate === element.animationTemplateName && element.animationTemplateName != ' ') {
      animationTemplateTarget = element.animationTemplateTarget;
      //pre - load the video to get de duration
      const loadVideo = file => new Promise((resolve, reject) => {
        try {
          let video = document.createElement('video')
          video.preload = 'metadata'
          video.onloadedmetadata = function() {
            resolve(this)
          }
          video.onerror = function() {
            reject("Invalid video. Please select a video file.")
          }
          video.src = file;
        } catch (e) {
          reject(e)
        }
      })

      let video = await loadVideo(element.animationTemplateFile);
      durationAnimation = video.duration;

      const play = async function(target) {
        await playAnimation(element.animationTemplateFile, element.animationTemplateSpeed, element.animationTemplateXScale,
          element.animationTemplateXScaleManual, element.animationTemplateYScale, element.animationTemplateYScaleManual,
          element.animationTemplateAngle, element.animationTemplateAnchorX, element.animationTemplateAnchorY,
          element.animationTemplateTarget, tokens[0], target, video);
      }
      //for each target selected
      game.user.targets.forEach((i, t) => {
        //play the saved animation
        if (canvas.fxmaster) {
          if (animationDelay != 0) {
            setTimeout(play, parseFloat(animationDelay) * 1000.0, t);
          } else {
            play(t);
          }
        } else {
          console.log(game.i18n.localize("SWADESFXVFX.NoFxMaster"));
        }
      });
    }
  }
  return durationAnimation;
}

// Play animation
export async function playAnimation(animationTemplateFile, animationTemplateSpeed, animationTemplateXScale, animationTemplateXScaleManual,
  animationTemplateYScale, animationTemplateYScaleManual, animationTemplateAngle,
  animationTemplateAnchorX, animationTemplateAnchorY, animationTemplateTarget, token, target, video) {

  return drawSpecialToward({
      file: animationTemplateFile,
      speed: animationTemplateSpeed,
      scale: {
        x: animationTemplateXScale == 'manual' ? animationTemplateXScaleManual : animationTemplateXScale,
        y: animationTemplateYScale == 'manual' ? animationTemplateYScaleManual : animationTemplateYScale,
      },
      ease: 'Linear',
      angle: animationTemplateAngle,
      anchor: {
        x: animationTemplateAnchorX,
        y: animationTemplateAnchorY
      }
      //playbackRate
      //animationDelay.start
      //animationDelay.end
    },
    (animationTemplateTarget == 'target') ? target : token,
    target,
    video
  );
}
