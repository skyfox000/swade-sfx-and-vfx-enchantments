export async function drawSpecialToward(effect, tok1, tok2, video) {

  if (!video) {
    //pre - load the video to get de duration
    const loadVideo = file => new Promise((resolve, reject) => {
      try {
        let video = document.createElement('video')
        video.preload = 'metadata'
        video.onloadedmetadata = function() {
          resolve(this)
        }
        video.onerror = function() {
          reject("Invalid video. Please select a video file.")
        }
        video.src = file;
      } catch (e) {
        reject(e)
      }
    })
    video = await loadVideo(effect.file);
  }

  const origin = {
    x: tok1.position.x + tok1.w / 2,
    y: tok1.position.y + tok1.h / 2
  }
  const effectData = mergeObject(effect, {
    position: {
      x: origin.x,
      y: origin.y
    }
  });
  const target = {
    x: tok2.position.x + tok2.w / 2,
    y: tok2.position.y + tok2.h / 2
  }

  // Compute angle
  const ray = new Ray(origin, target);
  effectData.distance = ray.distance - (1 - effectData.anchor.x) * tok2.width;
  effectData.rotation = ray.angle;

  const texture = PIXI.Texture.from(video);
  if (effectData.scale.x === "auto") {
    if (!effectData.distance) {
      effectData.scale.x = 1.0;
    } else {
      effectData.scale.x = ((effectData.distance + (tok2.w / 2) + (tok1.w / 2)) / texture.width)
    }
    if (effectData.scale.y === "auto") {
      if (!effectData.distance) {
        effectData.scale.y = 1.0;
      } else {
        effectData.scale.y = effectData.scale.x;
      }
    }
  }

  // And to other clients
  game.socket.emit('module.fxmaster', effectData);
  // Throw effect locally (fix for support of new 1.2.0 fxMaster)
  if (canvas.specials.playVideo) {
    canvas.specials.playVideo(effectData);
  } else {
    canvas.fxmaster.playVideo(effectData);
  }
}
