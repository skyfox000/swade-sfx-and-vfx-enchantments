
# SWADE Sfx & Vfx Enchantments

Make your weapons, spells, items, etc. feel more real in your SWADE games in foundry VTT, with different sounds and animation for each item, the "[FxMaster](https://foundryvtt.com/packages/fxmaster)" module is required to play the animation, the module is also compatible with the "[Token Magic FX](https://foundryvtt.com/packages/tokenmagic)" module effects.

## What can you do with this module

The module captures the moment in which the **selected token** makes an "*action*" roll for the item (shoot the weapon, cast the spell, etc.) and play animations, sound and/or effects (from "[Token Magic FX](https://foundryvtt.com/packages/tokenmagic)" if you have that module installed)

When you edit and item (spell, gun, etc.) , a new tab called "Animation and Sound" appears, there you can do:

- Associate an effect from the "[Token Magic FX](https://foundryvtt.com/packages/tokenmagic)" module that will be executed on the **token** that has the item configured when the item action is executed (shoot the gun, cast the spell, etc.)
- Associate an animation template (created in the settings) to play when the item action is executed (shoot the gun, cast the spell, etc.)
- Associate a sound template (created in settings) to play when the item action is executed (shoot the gun, cast the spell, etc.)
- Associate an effect from the "[Token Magic FX](https://foundryvtt.com/packages/tokenmagic)" module that will be executed on the **target** selected when the item action is executed when the item action is executed (shoot the gun, cast the spell, etc.)
- Export/download the sequence created to import in another item if you like.
- Import the animation and sound settings from another item that you export/download earlier (with drag and drop the json directly)

![Item configuration](https://gitlab.com/skyfox000/swade-sfx-and-vfx-enchantments/-/raw/master/images/settings_item.PNG)

***Note:*** the MagicToken options only appear if you have the module installed, if not, only the animation and sound template configuration will show.

## Configuration Settings

For playing an animation or sound, this module requires that you create animation or sound templates (that can be reused in different items)

![Settings](https://gitlab.com/skyfox000/swade-sfx-and-vfx-enchantments/-/raw/master/images/settings.PNG)

**Audio Templates:** In this option you can configure different animation templates that will later be associated and used in the items, you can also export/download the template list, or import one (drag and dropping the json directly) if you want to create or edit one manually.

![enter image description here](https://gitlab.com/skyfox000/swade-sfx-and-vfx-enchantments/-/raw/master/images/settings_sound.PNG)

**Animation Templates:** In this option you can configure different animation templates that will later be associated and used in the items, there are some specific stuff that you can configure to make the animation fill your needs, like auto-scale width and height, animation movement toward the targets, the starting point (anchor) in the token, etc. You can also export/download the template list, or import one (drag and dropping the json directly) if you want to create or edit one manually.

***Note:*** I really recommend that you **test/play** the animation (the button at the bottom of the page) to understand in more detail what each option do.

![Animation Settings](https://gitlab.com/skyfox000/swade-sfx-and-vfx-enchantments/-/raw/master/images/settings_animation.PNG)

## Compatibility

This module is created to work under the [SWADE](https://foundryvtt.com/packages/swade/) system and is also compatible with the "[Better Rolls 2 for Savage Worlds](https://foundryvtt.com/packages/betterrolls-swade2)" module, and with the ["Swade-tools](https://foundryvtt.com/packages/swade-tools)" module, I have tested it with other SWADE compatible modules and for now I did not find any problems, if you find any incompatibility with a module, please let me know.


## Known limitations

- If you are using the "[Token Magic FX](https://foundryvtt.com/packages/tokenmagic)" module, the effects list that can be selected for the items are from the **"TokenMagic Portfolio"** and **"TokenMagic Book of Fire"** macros compendium, if you want to use a custom effect, you have to include a macro in that compendium with your effect to appear in the list, using the same format, creating a *params* variable with the parameters for the effect and them a *TokenMagic.addUpdateFiltersOnSelected(params);* or a *TokenMagic.addFiltersOnSelected(params);* line to play it
- This module can replace my other module "Deadlands SWADE Gun Enchantments" (this one is more generic and more configurable) but you can use them both if you want, but try to not configure animations and sounds for the same item with the two modules, because the will play at the same time.

## Future TO-DO

- *Short Term:* I want to let people add custom TokenMagic effects to the list to select (they can do it now, but is not easy)
-  *Medium Term:*  allow to configure sounds and animations for hit, fail, critical hit and critical fail (global default ones, or per item)
-  *Medium Term:*  find a way to "patch" or assign animation-effects-sound sequence to various items at once, right now you can do that but item by item and not in a massive way.
-  *Long Term:* Try to add a *Trappings* management to spells, so you can configure animations and sounds for different Trappings, for now if you want different effects, animations and sounds for each Trapping you can create a copy of the spell and configure it differently in each one.
-  *Really long term:* try to add sequences with more than effect on token --> animation, sound --> effect on target, maybe using the [Sequencer](https://foundryvtt.com/packages/sequencer) module
- *Really long term:* maybe create a community for people to share their sequences, animations and sounds.

## Contribution
- If you'd like to support my work, feel free to leave a tip through [Paypal](https://www.paypal.com/donate?business=skyfox000@gmail.com&currency_code=EUR)
- if you want to contribute in the code you can create a branch and make a merge request.
- If you have any idea or request, open a requirement and I will try to review it (with the limitations that time allows me)

## License

- The source code is licensed under [GPL-3.0](https://www.gnu.org/licenses/gpl-3.0.html).
- A little part of this module is a modified code from the dependency module "[FxMaster](https://foundryvtt.com/packages/fxmaster)" module that is under [BSD-3 Clause "New" or "Revised"](https://choosealicense.com/licenses/bsd-3-clause/) license and have this specific [disclaimer](https://gitlab.com/mesfoliesludiques/foundryvtt-fxmaster/-/blob/master/LICENSE.md).
- Another little part of the module ia a modified code from the "[Automated Animations](https://foundryvtt.com/packages/autoanimations)" module that is under [BSD-3](https://opensource.org/licenses/BSD-3-Clause) and [MIT](https://opensource.org/licenses/MIT) license

## Thanks
- To "[FxMaster](https://foundryvtt.com/packages/fxmaster)" module, that I use to play the animations.
- To "[Automated Animations](https://foundryvtt.com/packages/autoanimations)" module for the item tab management part of the code that I took from them.
- to "[Token Magic FX](https://foundryvtt.com/packages/tokenmagic)" module that I use to play effects in the tokens and targets.
