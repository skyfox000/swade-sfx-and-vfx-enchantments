# Changelog

Important changes.

## [1.1.4]

### Added
- NONE
### Changed
- Fix bug for compatibility with last version of TokenMagic
### Removed
- NONE

## [1.1.3]

### Added
- NONE
### Changed
- Fix bug for compatibility with foundry V9
### Removed
- NONE

## [1.1.2]

### Added
- NONE
### Changed
- Fix bug for compatibility with SWADE tools
- Cleaning the code for a future big changes in the module
### Removed
- NONE

## [1.1.1]

### Added
- NONE
### Changed
- Fix bug for compatibility with SWADE 0.21.0
### Removed
- NONE

## [1.1.0]

### Added
- Support for "[SWADE Tools](https://foundryvtt.com/packages/swade-tools)" module.
- Export/Import Animation and Sound settings in the item tab.
- Export/Import Animation and Sound templates in settings.
- Now you can select effects from the Token Magic fire macros list compendium.
### Changed
- Fix a bug with the Animation and Sound tab that sometimes it disappeared.
- Fix a bug with the upload of sounds in the case of MP3 (now you can upload/select mp3 files).
- Fix some typos in English (not my native language).
### Removed
- NONE

## [1.0.0]

###

- Initial Release  
